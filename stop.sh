#!/bin/bash
PIDFILE=/var/run/nu-honeypot.pid
test -f "$PIDFILE" && kill $(< "$PIDFILE") && rm -f "$PIDFILE"

# ~ from twisted.internet import protocol, reactor
# ~ from twisted.python import log

# ~ class Echo(protocol.Protocol):
    # ~ def dataReceived(self, data):
        # ~ log.msg(data)
        # ~ self.transport.write(data)

# ~ class EchoFactory(protocol.Factory):
    # ~ def buildProtocol(self, addr):
        # ~ return Echo()

# Honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# import ConfigParser
import getpass
import logging
import os
import os.path
import re
import sys
import time
import unicodedata
import uuid

from twisted.conch.telnet import StatefulTelnetProtocol, TelnetTransport , AuthenticatingTelnetProtocol
from twisted.enterprise import adbapi
from twisted.internet import reactor , protocol ,endpoints
# ~ from twisted.internet import 
from twisted.internet.protocol import ServerFactory
from twisted.protocols.basic import LineReceiver
from twisted.python import log

import conf
import pof
# ~ from geoip import geolite2
from geolite2 import geolite2


class TelnetPotProtocol(AuthenticatingTelnetProtocol):

    state = 'User'
    def __init__(self):
        """Initialise the ConfigFactory
        and p0fFactory
        argument -- [ self ]
        return: object
        """
        self.delimiter = '\n'
        self.session = str(uuid.uuid1())
        self.firstLogin = True
        state = 'User'
        self.conf = conf.ConfigFactory()
        self.p0f = pof.p0fFactory()

    def telnet_password(self, line):
        """Saves the user details in mysql table
        argument -- [ self,line ]
        return: object
        """
        username, password = self.username, line
        password = password.rstrip('\r')
        #self.logger.debug(u'%s|lp|%s|%r,%r' % (self.session, self.transport.getHost().host,username,password))
        self.conf.log2db("INSERT INTO `users` (session,username,password) VALUES('" +
                         self.session+"','"+username+"','"+password+"')")

        if password.startswith('12345'):
            self.print_banner()
            self.state = "Command"
            self.print_banner()
            return 'Command'
        else:
            del self.username
            self.transport.write("\nAuthentication failed\n")
            self.transport.write("Username: ")
            self.state = "User"
            return 'User'

    def connectionMade(self):
        """Saves the session details details in mysql table
        argument -- [ self ]
        return: object
        """
        # ~ print(self.p0f.get_ip('127.0.0.1'))
        # ~ b"I received "+ data + b" from you\r\n"
        self.transport.write(b"Welcome To Babe web.\r\n")
        self.transport.write(b"Username: ")
        # ~ logger.debug(u'%s|new|%s|' % (self.session, self.transport.getHost().host))
        p0fResult = self.p0f.get_ip(self.transport.getPeer().host)
        # ~ print(geolite2.reader(self.transport.getPeer().host))
        reader = geolite2.reader()
        match = reader.get(self.transport.getPeer().host)
        print(match)
        cntry = match.country
        loc = match.location
        lat1 = str(loc[0])
        lon1 = str(loc[1])
        try:
            if not p0fResult["language"]:
                p0fResult["language"] = ""
            if not p0fResult["http_name"]:
                p0fResult["http_name"] = ""
            if not p0fResult["os_name"]:
                p0fResult["os_name"] = ""
            if not p0fResult["uptime_sec"]:
                p0fResult["uptime_sec"] = ""
            if not p0fResult["os_flavor"]:
                p0fResult["os_flavor"] = ""
            if not p0fResult["http_flavor"]:
                p0fResult["http_flavor"] = ""
        except:
            # self.transport.write("err")
            print('')
        self.conf.log2db("INSERT INTO `sessions` (session,ip,protocol,p0f_os,p0f_uptime,p0f_os_flavor,p0f_http_flavor,p0f_language,p0f_http_name,port,country,lat,lon) VALUES('"+self.session+"','"+self.transport.getPeer().host+"','telnet','" +
                         p0fResult["os_name"]+"','"+str(p0fResult["uptime_sec"])+"','"+p0fResult["os_flavor"]+"','"+p0fResult["http_flavor"]+"','"+p0fResult["language"]+"','"+p0fResult["http_name"]+"','"+str(self.transport.getHost().port)+"','"+cntry+"','"+lat1+"','"+lon1+"')")
    
    def connectionLost(self, reason):
        self.factory.connectionPool.remove(self.session)
        logging.info('Connection lost. Session ended')
    
    def telnet_User(self, line):

        # WTF, during the first time client sends binary so dirty code to cleanup
        if self.firstLogin == True:
            self.username = line.replace("'", '')
            self.username = self.username.replace(" ", "")
            self.username = re.sub(r'"', '', self.username)
            self.firstLogin = False
# Dirty code end
        else:
            self.username = line.replace("'", '')
            self.username = re.sub(r'"', '', self.username)

        self.username = self.username.rstrip('\r')
        self.transport.write("Password: ")

        return 'Password'

    def telnet_logged_in(self, line):
        self.transport.write("root@prod03:~# ")

    def enableRemote(self, option):
        return False

    def disableRemote(self, option):
        pass

    def enableLocal(self, option):
        return False

    def disableLocal(self, option):
        pass

    def print_banner(self):
        self.transport.write(
            "The programs included with the Debian GNU/Linux system are free software;\n")
        self.transport.write(
            "the exact distribution terms for each program are described in the\n")
        self.transport.write(
            "individual files in /usr/share/doc/*/copyright.\n\n")
        self.transport.write(
            "Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent\n")
        self.transport.write("permitted by applicable law.\n\n")
        self.transport.write("root@prod03:~# ")
    
    
    def telnet_command(self, cmd):
        """Saves the telnet commands in mysql table
        argument -- [ self,cmd ]
        return: object
        """
        self.conf.log2db(
            "INSERT INTO `cmds` (session,cmd) VALUES('"+self.session+"','"+cmd+"')")
        #logger.debug(u'%s|cmd|%s|%r' % (self.session, self.transport.getHost().host,cmd))
        if (cmd in ['quit\r', 'exit\r']):
            #log & close
            self.transport.loseConnection()
        elif (cmd.startswith('ls')):
            file = open("cmd/ls.txt")
            self.transport.write(file.read())
        elif (cmd.startswith('free')):
            file = open("cmd/free.txt")
            self.transport.write(file.read())
        elif (cmd.startswith('id')):
            file = open("cmd/id.txt")
            self.transport.write(file.read())
        elif (cmd.startswith('uname')):
            file = open("cmd/uname.txt")
            self.transport.write(file.read())
        elif (cmd.startswith('wget')):
            file = open("cmd/wget.txt")
            self.transport.write(file.read())
        elif (cmd.startswith('echo')):
            a = cmd.split(" ")
            self.transport.write(a[1])
            self.transport.write("\n")
        else:
            a = cmd.rstrip()
            self.transport.write("%r: command not found\n" % a)
        self.transport.write("root@prod03:~# ")


class telnetHoneyPotFactory(ServerFactory):
    # ~ print(Factory())
    

    def build_protocol(self, addr):
        return TelnetPotProtocol()
        
# ~ endpoints.serverFromString(reactor, "tcp:8023").listen(telnetHoneyPotFactory())

# ~ reactor.listenTCP(23, telnetHoneyPotFactory())
# ~ reactor.run()
# ~ reactor.run()

from twisted.application import internet, service
from twisted.conch.telnet import TelnetTransport
from twisted.internet.protocol import Factory
from echo import TelnetPotProtocol

factory = Factory()
factory.protocol = lambda: TelnetTransport(TelnetPotProtocol)
application = service.Application("echo")
serviceCollection = service.IServiceCollection(application)
echoService = internet.TCPServer(23, factory).setServiceParent(serviceCollection)

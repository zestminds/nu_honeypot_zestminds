apt-get update -y
#install required libs
apt-get install python-pip python-twisted python-mysqldb git libpcap-dev mysql-server -y
#install python deps
pip install p0f
pip install unirest
#used to reslove country
pip install python-geoip-geolite2
pip install mysql-connector-python
pip install mysql-connector-python --allow-external mysql-connector-python

#clone nu-honeypot
#~ git clone https://bitbucket.org/securenucleon/nu-honeypot.git

#clone p0f
git clone https://bitbucket.org/securenucleon/p0f.git
cd p0f/
#build p0f
./build.sh

#if all ok, run pof - you probably want to change the paths
./p0f -s /var/run/p0f.sock -d -o /var/log/p0f.log -f ./p0f.fp
